Categories:Development
License:GPLv3
Web Site:http://projects.sheimi.me/SGit
Source Code:https://github.com/sheimi/SGit
Issue Tracker:https://github.com/sheimi/SGit/issues
Donate:http://projects.sheimi.me/SGit

Auto Name:SGit
Summary:Git Client
Description:
A git client and text editor.
.

Repo Type:git
Repo:https://github.com/sheimi/SGit.git

Build:1.2.4.4,107
    commit=44d2a09009
    subdir=SGit

Build:1.3.0,108
    commit=v1.3.0
    gradle=yes

Build:1.3.1,109
    commit=v1.3.1-rc1
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.3.1
Current Version Code:109
