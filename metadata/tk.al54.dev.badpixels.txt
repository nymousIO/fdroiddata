Categories:System
License:GPLv3
Web Site:
Source Code:https://github.com/SibDev/BadPixels
Issue Tracker:https://github.com/SibDev/BadPixels/issues

Name:Bad Pixel Detector
Auto Name:Bad Pixels
Summary:Check for dead pixels
Description:
Check the screen for existence of so-called "dead pixels". So called bad and
defective pixels are defects of the electronic device which is perceiving or
reproducing the image and having pixel structure. A more detailed explanation is
[https://en.wikipedia.org/wiki/Defective_pixel in Wikipedia]. Also see the
[https://en.wikipedia.org/wiki/ISO_13406-2 ISO_13406-2] standard for more
information.
.

Repo Type:git
Repo:https://github.com/SibDev/BadPixels.git

Build:0.2,4
    disable=no-build
    commit=d073bde8fbaad9ea4b1c08047f4443686f7d7a41
    subdir=mobile
    target=android-22

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.2
Current Version Code:4
